# Scripting your narration

A good narration begins with a good script, a written outline of what you plan to say. Your script must correspond logically with and be timed perfectly to your video. Watch your edited video sequence as you write your script. Pay attention to the timecode (the exact second in the sequence of the images) as you write.

Here are some essential elements for scripting and recording a narration:

---

## WRITE FOR AUDIO

Write for the spoken word rather than for the written word. Your narration should be written in a near conversational tone rather than formal language to be read on the page. At the same time you need to sound like an authority, so diction and word choice should not be too colloquial.

---

## KEEP IT SIMPLE

Be clear and concise, stick to the story and don't try too hard to be "clever." Avoid complicated words and phrases that are tough to pronounce. Short sentences work best. If you deliver a long sentence, follow it with a short sentence.

---

## PROVIDE SPECIFICITY

You should avoid being too general or vague in your narration. The point of the narration is to provide context for anything that may cause confusion or raise a question for the listener. Be specific.

---

## USE THE PRESENT TENSE AND ACTIVE VOICE

You're writing for flow and to express what is going on now. Broadcast strives for immediacy. To convey this to the listener, use the active voice whenever possible. In English, try to use a subject-verb-object sentence structure. For example: "Police (subject) have arrested (verb) 21 activists (object) for staging a protest at Bryant Park on Saturday afternoon."

---

## WRITE TO THE PICTURES

TV and video audiences will *see* why something happened. In television, the phrase "write to tape" describes the way a story script is built around the visual images you have gathered.

---

## WRITE DESCRIPTIVELY

Audiences need to *imagine* the people, places and things in your story. Use descriptive verbs instead of adjectives. For example, if you say "he struts or saunters" you’re giving a picture without using an adjective. But don't let vivid, imagery-rich writing turn verbose. Use words sparingly.

---
