# Acoustics and Soundproofing

If you have ever had the experience of recording hollow or echo-laden sound, you know it can be distracting and make your audio hard to understand. Knowing how sound acts in space-- acoustics-- will help you better select a location or arrange one to best capture sound.

*RN_fig. 5 (BASIC ACOUSTICS: REPLACE WITH OUR OWN GRAPHIC]*

## "Live" Sound

When a sound wave hits a hard surface (plastic, glass, tile, stone walls, metal), little is absorbed, so the reflected sound is almost as loud as the original. The sound bouncing off the surface can actually sound brighter and sharper. When surroundings are reverberant, reflections are often heard seconds after the sound itself has stopped. In extreme cases, these sounds reflect as an echo.

---

## "Dead" Sound

When a sound wave hits a soft surface (curtains, couches, rugs), some of its energy is absorbed within the material. Higher notes are the most absorbed, so the sound reflected from this sort of surface is not only quieter than the original sound wave, but it lacks the higher frequencies. Its quality is more mellow, less resonant, even dull and muted. Certain soft materials absorb the sound so well that virtually none is reflected.

*RN_fig. 6 (Graphic illustrating room acoustics )*

In a place with many absorbent surfaces, both the original sound and any reflections can be significantly muffled. Under these "dead" conditions, the direct sound can be heard with few reflections from the surroundings. Even a loud noise such as a hand clap or a gunshot will not carry far and dies away quickly. When outside, in an open area, sound can be very dead. This is due to the air quickly absorbing the sound because there are few reflecting surfaces.

---

## Soundproofing Strategy

Acoustics often influence where the microphone is positioned. To avoid unwanted reflections in live surroundings, the mic needs to be placed relatively close to the subject. If working in dead surroundings, a close mic is necessary, because the sound does not carry well. When the surroundings are noisy, a close mic helps the voice (or other sound) to be heard clearly above the unwanted sounds.

However, there can also be problems if a mic is placed too close to the source. Sound quality is generally coarsened, and the bass can be overemphasized. The audience can become very aware of the noise of breathing, sibilants (**s** ), blasting from explosive **p’** s, **b** ’s, **t** ’s, and even clicks from the subject’s teeth striking together.

*RN_fig. 7 (‘Live’’ and "Dead’ soundproofing graphic)*

---
