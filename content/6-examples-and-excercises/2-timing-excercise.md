# Timing Exercise

Go back one of the examples in Part II. Listen to a short passage and guess how long it is. Use a stopwatch to check yourself. (Reminder: your wristwatch and cellphone probably have a stopwatch function.) Do it several times. Begin with small phrases first, then longer passages from documentaries and audiobooks, for example.



Once you're able to judge pretty well, record a short passage yourself and try and estimate the time. Listen back and see how you did. Gradually increase the length of your passage. Practice reading from our selection of practice scripts, until you develop that sense of time. Learn to approximate NOT ONLY the entire voice-over performance, but also the space **_between_** spoken passages.
