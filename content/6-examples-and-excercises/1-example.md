# Examples of Recording your Narration

Here are two examples of narrated videos shot with StoryMaker.

*RN_fig. 8 (StoryMaker examples from Libya)*

*RN_fig. 9 (StoryMaker examples from Libya)*

Watch and listen. Pay close attention to how the narrators use their voice to help convey emotion. Note how the audio "stitches together" the thread of the video sequences.

Watch and listen to this story about 5 Pointz in Long Island City, New York.

*RN_fig. 10 (Grafitti Story)*

Below is the script. It can be informative to see what a complex story looks like on paper. Pay special attention to the narration. The visuals are in the column in the left and the audio in the column on the right. The narration, as opposed to the interviews, is in all capital letters. Note how the reporter "sets up" an upcoming scene. Note how she provides context and background. Note how her question about the future ends the story.



<table>
  <tr>
    <td>




TS of spray pain can being shaken
MS of graffiti artist shaking spray paint can, graffiti wall in front of him.
MS spray painting on wall








Interview shot with 5 Pointz volunteer, Javier Rivera

WS of graffiti artist spray painting on wall, make a symbol-like circle.

TS of hand pressing down on spray paint can, releasing puffs of pink pain


WS of 5 Pointz, main open court

WS of graffiti wall, ‘I heart NY’

CU of graffiti work, same wall

WS of another graffiti wall on other side of court

MS of photographer taking photo of graffiti artist at work


MS of photographer taking photos of graffiti near subway track

MS of photographer taking photo against graffiti wall used as backdrop of interview with Evans


MS Evans interview shot

MS of guy turning in circle with camera

MS of Kanellos working in diner, grabbing food from kitchen window

MS interview shot of Kanellos, sitting in booth. 5 Pointz walls outside

MS of girl doing hand stand in front of graffiti wall

MS interview shot of Kanellos

TS Dead End sign
WS Dead end sign, subway line and 5 Pointz in background
MS of Citi Bank
WS of Citibank building in background with 5 Pointz in foreground

MS interview shot of Markman
Markman flipping through photographs of 5 Pointz on camera
MS interview shot of Markman


TS looking up at subway tracks. Train enters frame.
WS head on shot of train rounding the corner away from 5Pointz. Graffiti on subway structure visible.
CU of train pulling away, 7 sign

Interview shot of Evans




MS exterior of diner from 5Pointz, people and cars passing by
MS of car driving down street

Interview shot of Kanellos




MS of Kanellos tidying work station

Interview shot of Kanellos




MS of Rivera explain artwork to tourist



Interview shot of Rivera
MS of graffiti artist putting final touches on work
MS of graffiti artist taking photo of his work
Interview shot of Rivera

MS of Rivera and graffiti artist looking at camera, talking

Interview shot with Javier

WS of "Welcome to 5Pointz" door
CU of running fan above door and artwork
MS of someone opening “Welcome to 5 Pointz” door and closing it.
 </td>
    <td></td>
    <td>Anchor Lead: Plans to knock down graffiti mecca 5 Pointz in Long Island City has local residents and business owners talking about gentrification. Aine Pennello reports.


NAT SOUND of spray paint can being shaken




NAT SOUND of spray painting

RIVERA: Graffiti’s been around since the cavemen.

NAT SOUND of spray painting

RIVERA: Doodling on cave walls.


Native Americans, the pyramids, the Egyptians.



It’s all a form of graf because you’re telling your stories through symbols.


NAT SOUND of spray paint




UNLIKE THE ART OF GRAFFITI, 5 POINTZ IS RELATIVELY NEW.

SINCE OFFERING ITS WALLS TO

GRAFFITI ARTISTS IN 2001,


THE BUILDING HAS BECOME THE LARGEST PLACE IN THE WORLD WHERE ARTISTS


CAN LEGALLY SPRAY PAINT.

NAT SOUND of photo click and spray paint


AND RESIDENTS SAY IT’S BECOMING A PERMANENT

FIXTURE IN LONG ISLAND CITY.






EVANS: You’ll see packs of tourists come over here by a busload. You can’t help but notice it, you know.
It’s part of the neighborhood.

BUT LOCAL DINER MANAGER,
NICK KANELLOS, SAYS 5 POINTZ BRINGS IN MORE THAN JUST TOURISTS.

KANELLOS: It generates business, it generates activity in the area



and it brings life. Instead of having, you know, big buildings that

after six o’clock – click – they shut down and there’s nobody around.

BUT THE OWNER OF THE BUILDING HAS PLANS TO REPLACE 5 POINTZ WITH TWO LUXURY APARTMENT TOWERS AND A SHOPPING MALL, ONE OF THE LATEST GENTRIFICATION PROJECTS IN A NEIGHBORHOOD
KNOWN AS THE SECOND MANHATTAN.



NEIL MARKMAN: You know, I came to New York because I thought it was a place
for expression and I thought it was a place that was much different than any other city. And with all the

gentrification it just feels like it’s becoming like any other large city or any mall in America. (sound of train in background)

NAT sound of train

A TREND THAT’S CAUSING A DIVIDE BETWEEN LOCAL OLD-TIMERS


AND INCOMING RESIDENTS.


EVANS: We, of the local residents don’t have much interaction with them of the big condos. They don’t even recognize the neighborhood really. It’s just a place that they pass through to get to their condo.

GENTRIFICATION CAN ALSO LEAD TO MORE PRACTICAL ISSUES. KANELLOS SAYS

HE SPENDS 30 TO 40 MINUTES CIRCLING THE BLOCK EVERY DAY TO FIND PARKING.

KANELLOS: Everybody wants to put up towers but nobody’s thinking about people driving. So there’s been less and less and less and eventually it’s going to be like the city and no one can come over here with a car.

BUT HE ADMITS THAT GENTRIFICATION IS A MIXTURE OF GOOD AND BAD.

KANELLOS: Yes, it has helped the area, all this developments and what they do. On the other hand, it’s taking the Long Island City as a neighborhood away and making it a semi-commercial area.

5POINTZ VOLUNTEER, JAVIER RIVERA, SAYS MUCH LIKE THE BUILDINGS IN THE NEIGHBORHOOD, GRAFFITI IS TEMPORARY.


RIVERA: I’ve seen stuff here last not even 20 minutes.
You do something, you get your photograph, your little video,
and the minute you go up to that corner and you leave that dead end,

the wall is given to another artists.

SO WHAT WOULD THE LOSS OF 5 POINTZ MEAN TO GRAFFITI ARTISTS LIKE RIVERA?

RIVERA: It would hurt,
it would hurt. But I’m a New Yorker born and raised. I’ve seen buildings come and go, I’ve had
careers come and go. So, I will adjust.

I will find another

something to, you know, occupy my time until I no longer walk this earth.


AINE PENNELLO, NEW YORK</td>
  </tr>
</table>


### Some additional online resources

* [http://airmedia.org/PageInfo.php?PageID=206](http://airmedia.org/PageInfo.php?PageID=206)

* [http://airmedia.org/PageInfo.php?PageID=199](http://airmedia.org/PageInfo.php?PageID=199)

* [http://mediahelpingmedia.org/training-resources/journalism-basics/646-tips-for-writing-radio-news-scripts](http://mediahelpingmedia.org/training-resources/journalism-basics/646-tips-for-writing-radio-news-scripts)

* [http://www.videomaker.com/article/15804-10-ways-to-build-your-voice-over-skills](http://www.videomaker.com/article/15804-10-ways-to-build-your-voice-over-skills)
