# Create a Narration Exercise

**NOTE: HOW WILL PARTICIPANTS UPLOAD VID TO STORYMAKER?**

*RN_fig. 11 (gym_video)
*

First write a script and then, using StoryMaker, record a narration for this short video about making a cup of espresso. StoryMaker has an easy-to-use feature for recording Voice overs/Narrations. First watch the tutorial in StoryMaker and then give it a try.

### Shooting and Narration Exercise

For the following three exercises, use StoryMaker to first shoot your video and then record the narration using the feature for recording narration.

#### (Beginner) Personal Essay Video with Voiceover

Take the viewer on a tour of a scene in your life. It could be your home, your workplace, or somewhere you spend a lot of time. *Use a simple-story 5 shot scene template.*

#### (Intermediate) Event Voiceover

Record a voiceover for a breaking news event. Capture shots of the action from multiple angles. Assemble a sequence of action. Use voiceover to create a feeling of seamless transition between shots; "cover your edit." *Use the Breaking News **template**.*

#### (Advanced) News-channel Style Voiceover

Make a report in the style of cable news channels like Al Jazeera. Use a multi-scene story template. Make sure to include interviews and voices other than your own. *Choose your own template.*
