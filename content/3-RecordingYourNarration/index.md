# Recording the Narration

After you have written a draft of your script, it is important that you practice it, especially difficult words and phrases, or places where you run out of breath. You may wish to make notations where you want a pause or to add a particular intonation to your voice. Do not hesitate to change the script to make your delivery easier.

Be sure to breathe deeply and be relaxed (maybe have a sip of water) to clear your throat. Speak clearly and enunciate. Be sure to pronounce important letters and syllables. Express energy, emotion and emphasize important points through rhythm and fluctuation of voice -- rising or falling voice. A good narrator conveys importance and drama through the tone and rhythm of the delivery. Some people prefer to stand when recording narration to better achieve these things.

When writing a narration for video, called writing to tape, timing is everything. Good audio journalists can estimate the number of words they can deliver down to the second. The following word count timings should assist you in determining how many words will comfortably fit within a specified amount of time in a spot.

Note also that the English language was used to make these estimates. Other languages will have different results.

| **WORDS** | **SECONDS** |
| :-------: | ----------: |
| 7         | 3 seconds   |
| 12        | 5 seconds   |
| 17        | 7 seconds   |
| 23        | 10 seconds  |
| 35        | 15 seconds  |
| 70        | 30 seconds  |
| 140       | 60 seconds  |


With enough practice, you develop an internal clock that tells you very closely when, say, 30 seconds is up.

---
