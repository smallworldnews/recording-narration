# Introduction to recording narration and voiceovers

![](images/intro.jpg)

*In the "Planning your Video" module, we learned how to make a sequence of video images that tell a story. Let’s add a voiceover, sometimes called narration, to the sequence to provide the audience additional information and context to the images.*

The best narrations do more than merely describe what is in the images. They elaborate on and explain them to help the viewer better understand the issue. Like the visual sequence, a good script tells a story. It has an arc -- a beginning, middle and end -- that complements the video story.

Sometimes a narration and the video it accompanies are written in three acts like a theater performance:


> **Act I** - introduces the issue
> **Act II** - provides more detail about the issue, including opposing opinions
> **Act III** - provides resolution -- if there is resolution -- or describes what will happen next.


---

**A very good narration can carry a story without any images at all!**

---

![](images/coffee.png)

![](images/yt_20.png) [*Introduction to Narration: Coffee Example*](https://www.youtube.com/watch?v=ff2pubrN-Rc)

In addition to telling story, the best narrations do the following:

* Explain or provide context to the visuals without merely stating what is in the image.

* "Set up" or introduce a visual before or just as it appears on the screen.

* Provide essential background information to cover for visuals you do not have.

* Provide a conclusion to the story, sometimes called an "out" or “outro”.

---
