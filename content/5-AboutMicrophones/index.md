# About Microphones

An essential tool for the mobile journalist recording audio is a good microphone. Mobile devices can record quality audio, but most built-in microphones are not that great. You need a external microphone to connect to a mobile device.

<!-- more -->

There are many, many kinds of microphones for every possible situation. You can generally find a mic that is designed to work on a mobile device with a mini plug, such as the [iRig Cast](http://www.ikmultimedia.com/products/irigmiccast/). If not, make sure to have an adapter, like one shown here:

![](images/adapters.jpg)

Here is a link to a Small World News Post about the [variety of microphones](http://smallworldnews.tv/blog/a-basic-guide-to-using-microphones-with-mobiles/) for all kinds of situations.

![](images/ExternalMicExamples.png)

![](images/yt_20.png) [*Introduction to Narration: Coffee Example*](https://www.youtube.com/watch?v=J0n26RswT-c)

Watch this video and listen to how a good microphone can dramatically improve quality. For narration you will likely use a hand-held mic, a mic like the iRig or, if nothing else works the microphone in your mobile device or the microphone in your headset.
