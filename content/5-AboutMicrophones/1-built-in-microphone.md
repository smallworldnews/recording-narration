# Built-in microphone

![](images/built-in-mic.jpg)

This microphone is a part of any video recording device. Only depend on the built-in mic if you are concerned about safety and don’t wish to be observed.

## ADVANTAGES

* Convenient because it is always ready to record.

* Doesn’t call attention to your recording.

## DISADVANTAGES

* Even a slight wind may create a lot of noise with these microphones.
* All ambient sound is recorded, so background noise may be very loud.
* People’s voices, especially when speaking quietly, may not be recorded.

---
