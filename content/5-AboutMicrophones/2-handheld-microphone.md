# Handheld microphone

![](images/handheld-mic.jpg)

This microphone is commonly seen in the hand of correspondents reporting live at the scene of events. The handheld mic works well for correspondents reporting on-camera, or conducting interviews in a public area, on-camera.

## ADVANTAGES

* Records ambient noise with your narration, which is sometimes desirable.
* Very common and easy to find brackets or holders.

## DISADVANTAGES

* The correspondent must maintain control of the microphone at all times.
* The mic can only record a limited area.

---
