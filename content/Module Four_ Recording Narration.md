**Social-First ReportingModule Four | Recording Narration**

[[TOC]]

# **Module Introduction**

This module on recording narration is one of four modules intended to help start-up and existing news organizations develop the skills required to produce high quality journalism for today’s mobile media landscape. The four modules, which are accompanied by "Crafting a Social-first News Strategy," an overview set of guidelines on developing an editorial plan, cover the following topics:

* Module One: Planning your Coverage

* Module Two: Photography for News

* Module Three: Scripting and Shooting Video

* Module Four: Recording Narration

The modules are designed to be hands-on and to teach the core multimedia skills reporters and editors need to possess to do the job in a media environment where people get much of their news through mobile devices and social sites, often contributing content to a story as it develops. This is called ‘Social-first’ news and is quickly changing how the news is reported and distributed.

Each module in the series has four parts:

I. An Introduction to the topic

II. A collection of professional examples to illustrate the concept, along with additional resources that may be helpful to reporters and their news organizations.

III. Exercises and quizzes that emphasize application of the skill.

IV. Critique of an assignment by a mentor or staff person,

Each module includes a package of multimedia example content and can be completed by reporters, editors and others independently. In total, steps one through three should take between 30 minutes and two hours to complete. The time required for step four, the assignment, will vary.

The modules are intended for news organizations using StoryMaker, a cutting-edge Android application that helps journalists produce, edit and distribute multimedia content with mobile devices. StoryMaker can be downloaded for free from the Google Play Store.

The Modules and StoryMaker were produced by Small World News (SWN) for the Institute for War and Peace Reporting (IWPR). Small World News started in 2005 with its first project ‘Alive in Baghdad,’ which initially produced weekly video packages on citizens’ daily life. SWN and the ‘Alive in…’ project has expanded globally and now focuses on creating social-first media projects in emerging markets. SWN has trained hundreds of journalists and activists around the globe, and has produced several media guides, including a guide to safety for reporters and a comprehensive basic journalism course as part of the StoryMaker mobile application.

In 2011 Small World News partnered with the Guardian Project, Free Press Unlimited, and Scal.io, to produce StoryMaker, a secure journalism production and learning tool for journalists using mobile tools. Small World News’s stakeholders are journalists, activists, and citizens anywhere in the world working to tell stories better.

IWPR gives voice to people at the frontlines of conflict, crisis and change. From Afghanistan to Zimbabwe, IWPR helps people in the world's most challenging environments have the information they need to drive positive changes in their lives — holding government to account, demanding constructive solutions, strengthening civil society and securing human rights. Amid war, dictatorship, and political transition, IWPR forges the skills and capacity of local journalism, strengthens local media institutions and engages with civil society and governments to ensure that information achieves impact.

SWN and IWPR hope these modules and the skills they introduce will help journalists and the communities in which they live and work produce exciting stories that embody the highest standards of journalism with the newest tools. Good stories can change the world.* * *


# Module Four: Recording Narration

* * *


## Part One | An Introduction to recording narration and voiceovers

### Overview

In the "Planning your Video" module, we learned how to make a sequence of video images that tell a story. Let’s add a voiceover, sometimes called narration, to the sequence to provide the audience additional information and context to the images.

Scripting and recording a good voiceover is a professional skill that is developed over time, with practice. By following this lesson, you will begin to master this seemingly simple-- but actually quite difficult -- part of making a video.

The best narrations do more than merely describe what is in the images. They elaborate on and explain them to help the viewer better understand the issue. Like the visual sequence, a good script tells a story. It has an arc -- a beginning, middle and end -- that complements the video story. 

Sometimes a narration and the video it accompanies are written in three acts like a theater performance:

* Act I - introduces the issue

* Acti II - provides more detail about the issue, including opposing opinions

* Acti III - provides resolution -- if there is resolution -- or describes what will happen next.

A very good narration can carry a story without any images at all!

In addition to telling story, the best narrations do the following:

* Explain or provide context to the visuals without merely stating what is in the image.

* "Set up" or introduce a visual before or just as it appears on the screen.

* Provide essential background information to cover for visuals you do not have.

* Provide a conclusion to the story, sometimes called an "out" or “outro”.

*RN_fig. 1 (coffee_Gwen Example Narration)*

### Scripting your narration

A good narration begins with a good script, a written outline of what you plan to say. Your script must correspond logically with and be timed perfectly to your video. Watch your edited video sequence as you write your script. Pay attention to the timecode (the exact second in the sequence of the images) as you write.

Here are some essential elements for scripting and recording a narration:

**Write for audio**.

Write for the spoken word rather than for the written word. Your narration should be written in a near conversational tone rather than formal language to be read on the page. At the same time you need to sound like an authority, so diction and word choice should not be too colloquial. 

**Keep it simple**

Be clear and concise, stick to the story and don't try too hard to be "clever." Avoid complicated words and phrases that are tough to pronounce. Short sentences work best. If you deliver a long sentence, follow it with a short sentence.

**Provide specificity**

You should avoid being too general or vague in your narration. The point of the narration is to provide context for anything that may cause confusion or raise a question for the listener. Be specific. 

**Use the present tense and active voice**

You're writing for flow and to express what is going on now. Broadcast strives for immediacy. To convey this to the listener, use the active voice whenever possible. In English, try to use a subject-verb-object sentence structure. For example: "Police (subject) have arrested (verb) 21 activists (object) for staging a protest at Bryant Park on Saturday afternoon."

******Write to the pictures**

TV and video audiences will *see* why something happened. In television, the phrase "write to tape" describes the way a story script is built around the visual images you have gathered. 

**Write descriptively**

Audiences need to *imagine* the people, places and things in your story. Use descriptive verbs instead of adjectives. For example, if you say "he struts or saunters" you’re giving a picture without using an adjective. But don't let vivid, imagery-rich writing turn verbose. Use words sparingly.

### Recording the Narration

After you have written a draft of your script, it is important that you practice it, especially difficult words and phrases, or places where you run out of breath. You may wish to make notations where you want a pause or to add a particular intonation to your voice. Do not hesitate to change the script to make your delivery easier.

Be sure to breathe deeply and be relaxed (maybe have a sip of water) to clear your throat. Speak clearly and enunciate. Be sure to pronounce important letters and syllables. Express energy, emotion and emphasize important points through rhythm and fluctuation of voice -- rising or falling voice. A good narrator conveys importance and drama through the tone and rhythm of the delivery. Some people prefer to stand when recording narration to better achieve these things. 

When writing a narration for video, called writing to tape, timing is everything. Good audio journalists can estimate the number of words they can deliver down to the second. The following word count timings should assist you in determining how many words will comfortably fit within a specified amount of time in a spot. 

 

Note also that the English language was used to make these estimates. Other languages will have different results.

<table>
  <tr>
    <td>WORDS</td>
    <td>SECONDS</td>
  </tr>
  <tr>
    <td>7 words</td>
    <td>3 seconds</td>
  </tr>
  <tr>
    <td>12 words</td>
    <td>5 seconds</td>
  </tr>
  <tr>
    <td>17 words</td>
    <td>7 seconds</td>
  </tr>
  <tr>
    <td>23 words</td>
    <td>10 seconds</td>
  </tr>
  <tr>
    <td>35 words</td>
    <td>15 seconds</td>
  </tr>
  <tr>
    <td>70 words</td>
    <td>30 seconds</td>
  </tr>
  <tr>
    <td>140 words</td>
    <td>60 seconds</td>
  </tr>
</table>


With enough practice, you develop an internal clock that tells you very closely when, say, 30 seconds is up.

### Technical tips for recording sound

Remember that sound is half the story. Paying close attention to the quality of the sound you’re recording is an easy way to improve the overall quality of your video, and increase the impact of the story you have to tell.

Below are some essential tips for better audio quality.

**Quiet is Key.**

Find a quiet place to record your audio. Avoid room tone -- the sound of fans (turn them off), refrigerators (unplug them) and even traffic (close the window). All of these will seem amplified and distracting to your audience. If you have any sound problem or noise interference that cannot be solved, consider relocation. Background noise and poor audio quality can ruin your audio. Audiences are more likely to tolerate bad video than they are bad audio.

**Always use headphones.**

It cannot be stressed enough. Headphones allow you to hear everything that your camera or recorder picks up (or nothing, if you forgot to switch on your mic). You will hear whether you are recording properly, and quickly be able to detect and solve any problems you might encounter, such as wind or additional noise. Wind interference, for example, can ruin your interview and render your entire shot worthless.

**Test Recording First.**

Check your sound immediately after recording. This will ensure that your equipment is in good working order. When conducting an interview, be sure to conduct a test, because once you are recording live, you can’t recreate what you failed to record!

**Turn your back on noise.**

If you are recording your narration on location and picking up noise you don’t want, your body can form an obstacle between you and the noise. This may be less effective with an omni-directional microphone. If you record with a cardioid or shotgun microphone, you can mitigate sound considerably by turning your back (and repositioning your subject accordingly) towards the source of noise.

**Stabilize and protect the mic.**

Always use a shock-mount to protect your microphone from movement. When holding the a microphone take care not to move your hand to avoid the noise of your hand on the handle. If outdoors, protect your microphone from wind (use a windscreen) or rain. Microphones are delicate and pick up any sound, including vibration and wind.

### About Microphones

An essential tool for the mobile journalist recording audio is a good microphone. Mobile devices can record quality audio, but most built-in microphones are not that great. You need a external microphone to connect to a mobile device. 

There are many, many kinds of microphones for every possible situation. You can generally find a mic that is designed to work on a mobile device with a mini plug, such as the [iRig Cast](http://www.ikmultimedia.com/products/irigmiccast/). If not, make sure to have an adapter, like one shown here:

*RN_fig. 2 (image of mic and connector)*

Here is a link to a Small World News Post about the [variety of microphones](http://smallworldnews.tv/blog/a-basic-guide-to-using-microphones-with-mobiles/) for all kinds of situations. Watch this video and listen to how a good microphone can dramatically improve quality.

For narration you will likely use a hand-held mic, a mic like the iRig or, if nothing else works the microphone in your mobile device or the microphone in your headset.

RN_fig. 3 (image of Galaxy with mic bracket/built-in mic)

#### Built-in microphone

This microphone is a part of any video recording device. Only depend on the built-in mic if you are concerned about safety and don’t wish to be observed.

Advantages:

* Convenient because it is always ready to record.

* Doesn’t call attention to your recording.

Disadvantages:

* Even a slight wind may create a lot of noise with these microphones.

* All ambient sound is recorded, so background noise may be very loud.

* People’s voices, especially when speaking quietly, may not be recorded.

RN_fig. 4 (image of hand-held mic)

#### Handheld microphone

This microphone is commonly seen in the hand of correspondents reporting live at the scene of events. The handheld mic works well for correspondents reporting on-camera, or conducting interviews in a public area, on-camera.

Advantages:

* Records ambient noise with your narration, which is sometimes desirable.

* Very common and easy to find brackets or holders.

Disadvantages:

* The correspondent must maintain control of the microphone at all times.

* The mic can only record a limited area.

### Acoustics and Soundproofing

If you have ever had the experience of recording hollow or echo-laden sound, you know it can be distracting and make your audio hard to understand. Knowing how sound acts in space-- acoustics-- will help you better select a location or arrange one to best capture sound.

*RN_fig. 5 (BASIC ACOUSTICS: REPLACE WITH OUR OWN GRAPHIC]*

**"Live" Sound**

When a sound wave hits a hard surface (plastic, glass, tile, stone walls, metal), little is absorbed, so the reflected sound is almost as loud as the original. The sound bouncing off the surface can actually sound brighter and sharper. When surroundings are reverberant, reflections are often heard seconds after the sound itself has stopped. In extreme cases, these sounds reflect as an echo. 

**"Dead" Sound**

When a sound wave hits a soft surface (curtains, couches, rugs), some of its energy is absorbed within the material. Higher notes are the most absorbed, so the sound reflected from this sort of surface is not only quieter than the original sound wave, but it lacks the higher frequencies. Its quality is more mellow, less resonant, even dull and muted. Certain soft materials absorb the sound so well that virtually none is reflected.

*RN_fig. 6 (Graphic illustrating room acoustics )*

In a place with many absorbent surfaces, both the original sound and any reflections can be significantly muffled. Under these "dead" conditions, the direct sound can be heard with few reflections from the surroundings. Even a loud noise such as a hand clap or a gunshot will not carry far and dies away quickly. When outside, in an open area, sound can be very dead. This is due to the air quickly absorbing the sound because there are few reflecting surfaces.

### Soundproofing Strategy

Acoustics often influence where the microphone is positioned. To avoid unwanted reflections in live surroundings, the mic needs to be placed relatively close to the subject. If working in dead surroundings, a close mic is necessary, because the sound does not carry well. When the surroundings are noisy, a close mic helps the voice (or other sound) to be heard clearly above the unwanted sounds.

However, there can also be problems if a mic is placed too close to the source. Sound quality is generally coarsened, and the bass can be overemphasized. The audience can become very aware of the noise of breathing, sibilants (**s** ), blasting from explosive **p’** s, **b** ’s, **t** ’s, and even clicks from the subject’s teeth striking together.

*RN_fig. 7 (‘Live’’ and "Dead’ soundproofing graphic)*

## Part Two | Examples of Recording your Narration

Here are two examples of narrated videos shot with StoryMaker. 

*RN_fig. 8 (StoryMaker examples from Libya)*

*RN_fig. 9 (StoryMaker examples from Libya)*

Watch and listen. Pay close attention to how the narrators use their voice to help convey emotion. Note how the audio "stitches together" the thread of the video sequences.

Watch and listen to this story about 5 Pointz in Long Island City, New York.

*RN_fig. 10 (Grafitti Story)*

Below is the script. It can be informative to see what a complex story looks like on paper. Pay special attention to the narration. The visuals are in the column in the left and the audio in the column on the right. The narration, as opposed to the interviews, is in all capital letters. Note how the reporter "sets up" an upcoming scene. Note how she provides context and background. Note how her question about the future ends the story.

 

<table>
  <tr>
    <td>


 
 
TS of spray pain can being shaken
MS of graffiti artist shaking spray paint can, graffiti wall in front of him.
MS spray painting on wall
 
 
 
 
 
 
 
 
Interview shot with 5 Pointz volunteer, Javier Rivera
 
WS of graffiti artist spray painting on wall, make a symbol-like circle.
 
TS of hand pressing down on spray paint can, releasing puffs of pink pain
 
 
WS of 5 Pointz, main open court
 
WS of graffiti wall, ‘I heart NY’
 
CU of graffiti work, same wall
 
WS of another graffiti wall on other side of court
 
MS of photographer taking photo of graffiti artist at work
 
 
MS of photographer taking photos of graffiti near subway track
 
MS of photographer taking photo against graffiti wall used as backdrop of interview with Evans
 
 
MS Evans interview shot
 
MS of guy turning in circle with camera
 
MS of Kanellos working in diner, grabbing food from kitchen window
 
MS interview shot of Kanellos, sitting in booth. 5 Pointz walls outside
 
MS of girl doing hand stand in front of graffiti wall
 
MS interview shot of Kanellos
 
TS Dead End sign
WS Dead end sign, subway line and 5 Pointz in background
MS of Citi Bank
WS of Citibank building in background with 5 Pointz in foreground
 
MS interview shot of Markman
Markman flipping through photographs of 5 Pointz on camera
MS interview shot of Markman
 
 
TS looking up at subway tracks. Train enters frame.
WS head on shot of train rounding the corner away from 5Pointz. Graffiti on subway structure visible.
CU of train pulling away, 7 sign
 
Interview shot of Evans
 
 
 
 
MS exterior of diner from 5Pointz, people and cars passing by
MS of car driving down street
 
Interview shot of Kanellos
 
 
 
 
MS of Kanellos tidying work station
 
Interview shot of Kanellos
 
 
 
 
MS of Rivera explain artwork to tourist
 
 
 
Interview shot of Rivera
MS of graffiti artist putting final touches on work
MS of graffiti artist taking photo of his work
Interview shot of Rivera
 
MS of Rivera and graffiti artist looking at camera, talking
 
Interview shot with Javier
 
WS of "Welcome to 5Pointz" door
CU of running fan above door and artwork
MS of someone opening “Welcome to 5 Pointz” door and closing it.
 </td>
    <td></td>
    <td>Anchor Lead: Plans to knock down graffiti mecca 5 Pointz in Long Island City has local residents and business owners talking about gentrification. Aine Pennello reports.
 

NAT SOUND of spray paint can being shaken
 
 
 
 
NAT SOUND of spray painting
 
RIVERA: Graffiti’s been around since the cavemen.
 
NAT SOUND of spray painting
 
RIVERA: Doodling on cave walls.
 
 
Native Americans, the pyramids, the Egyptians.
 
 
 
It’s all a form of graf because you’re telling your stories through symbols.
 
 
NAT SOUND of spray paint
 
 
 
 
UNLIKE THE ART OF GRAFFITI, 5 POINTZ IS RELATIVELY NEW.
 
SINCE OFFERING ITS WALLS TO
 
GRAFFITI ARTISTS IN 2001,
 
 
THE BUILDING HAS BECOME THE LARGEST PLACE IN THE WORLD WHERE ARTISTS
 
 
CAN LEGALLY SPRAY PAINT.
 
NAT SOUND of photo click and spray paint
 
 
AND RESIDENTS SAY IT’S BECOMING A PERMANENT
 
FIXTURE IN LONG ISLAND CITY.

 
 
 
 
 
EVANS: You’ll see packs of tourists come over here by a busload. You can’t help but notice it, you know.
It’s part of the neighborhood.
 
BUT LOCAL DINER MANAGER,
NICK KANELLOS, SAYS 5 POINTZ BRINGS IN MORE THAN JUST TOURISTS.
 
KANELLOS: It generates business, it generates activity in the area
 
 
 
and it brings life. Instead of having, you know, big buildings that
 
after six o’clock – click – they shut down and there’s nobody around.
 
BUT THE OWNER OF THE BUILDING HAS PLANS TO REPLACE 5 POINTZ WITH TWO LUXURY APARTMENT TOWERS AND A SHOPPING MALL, ONE OF THE LATEST GENTRIFICATION PROJECTS IN A NEIGHBORHOOD
KNOWN AS THE SECOND MANHATTAN.
 
 
 
NEIL MARKMAN: You know, I came to New York because I thought it was a place
for expression and I thought it was a place that was much different than any other city. And with all the
 
gentrification it just feels like it’s becoming like any other large city or any mall in America. (sound of train in background)
 
NAT sound of train
 
A TREND THAT’S CAUSING A DIVIDE BETWEEN LOCAL OLD-TIMERS
 
 
AND INCOMING RESIDENTS.
 
 
EVANS: We, of the local residents don’t have much interaction with them of the big condos. They don’t even recognize the neighborhood really. It’s just a place that they pass through to get to their condo.
 
GENTRIFICATION CAN ALSO LEAD TO MORE PRACTICAL ISSUES. KANELLOS SAYS
 
HE SPENDS 30 TO 40 MINUTES CIRCLING THE BLOCK EVERY DAY TO FIND PARKING. 
 
KANELLOS: Everybody wants to put up towers but nobody’s thinking about people driving. So there’s been less and less and less and eventually it’s going to be like the city and no one can come over here with a car.
 
BUT HE ADMITS THAT GENTRIFICATION IS A MIXTURE OF GOOD AND BAD.
 
KANELLOS: Yes, it has helped the area, all this developments and what they do. On the other hand, it’s taking the Long Island City as a neighborhood away and making it a semi-commercial area.
 
5POINTZ VOLUNTEER, JAVIER RIVERA, SAYS MUCH LIKE THE BUILDINGS IN THE NEIGHBORHOOD, GRAFFITI IS TEMPORARY.
 
 
RIVERA: I’ve seen stuff here last not even 20 minutes.
You do something, you get your photograph, your little video,
and the minute you go up to that corner and you leave that dead end,
 
the wall is given to another artists.
 
SO WHAT WOULD THE LOSS OF 5 POINTZ MEAN TO GRAFFITI ARTISTS LIKE RIVERA?
 
RIVERA: It would hurt,
it would hurt. But I’m a New Yorker born and raised. I’ve seen buildings come and go, I’ve had
careers come and go. So, I will adjust.
 
I will find another
 
something to, you know, occupy my time until I no longer walk this earth.
 
 
AINE PENNELLO, NEW YORK</td>
  </tr>
</table>


### Some additional online resources

* [http://airmedia.org/PageInfo.php?PageID=206](http://airmedia.org/PageInfo.php?PageID=206)

* [http://airmedia.org/PageInfo.php?PageID=199](http://airmedia.org/PageInfo.php?PageID=199)

* [http://mediahelpingmedia.org/training-resources/journalism-basics/646-tips-for-writing-radio-news-scripts](http://mediahelpingmedia.org/training-resources/journalism-basics/646-tips-for-writing-radio-news-scripts)

* [http://www.videomaker.com/article/15804-10-ways-to-build-your-voice-over-skills](http://www.videomaker.com/article/15804-10-ways-to-build-your-voice-over-skills)

## Part Three | Application / Exercises 

### **Timing Exercise** 

Go back one of the examples in Part II. Listen to a short passage and guess how long it is. Use a stopwatch to check yourself. (Reminder: your wristwatch and cellphone probably have a stopwatch function.) Do it several times. Begin with small phrases first, then longer passages from documentaries and audiobooks, for example.

 

Once you're able to judge pretty well, record a short passage yourself and try and estimate the time. Listen back and see how you did. Gradually increase the length of your passage. Practice reading from our selection of practice scripts, until you develop that sense of time. Learn to approximate NOT ONLY the entire voice-over performance, but also the space **_between_** spoken passages.

### Create a Narration Exercise

**NOTE: HOW WILL PARTICIPANTS UPLOAD VID TO STORYMAKER?**

*RN_fig. 11 (gym_video)*

First write a script and then, using StoryMaker, record a narration for this short video about making a cup of espresso. StoryMaker has an easy-to-use feature for recording Voice overs/Narrations. First watch the tutorial in StoryMaker and then give it a try.

### Shooting and Narration Exercise

For the following three exercises, use StoryMaker to first shoot your video and then record the narration using the feature for recording narration.

#### (Beginner) Personal Essay Video with Voiceover

Take the viewer on a tour of a scene in your life. It could be your home, your workplace, or somewhere you spend a lot of time. *Use a simple-story 5 shot scene template.* 

#### (Intermediate) Event Voiceover

Record a voiceover for a breaking news event. Capture shots of the action from multiple angles. Assemble a sequence of action. Use voiceover to create a feeling of seamless transition between shots; "cover your edit." *Use the Breaking News **template**.*

#### (Advanced) News-channel Style Voiceover

Make a report in the style of cable news channels like Al Jazeera. Use a multi-scene story template. Make sure to include interviews and voices other than your own. *Choose your own template.*

## Part Four | On Assignment

Participants Will Be Required to Record and Post One (or more) Assignments for Critique.

