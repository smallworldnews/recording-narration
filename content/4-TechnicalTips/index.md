# Technical tips for recording sound

Remember that sound is half the story. Paying close attention to the quality of the sound you’re recording is an easy way to improve the overall quality of your video, and increase the impact of the story you have to tell.

### Below are some essential tips for better audio quality.

---

## QUIET IS KEY

Find a quiet place to record your audio. Avoid room tone -- the sound of fans (turn them off), refrigerators (unplug them) and even traffic (close the window). All of these will seem amplified and distracting to your audience. If you have any sound problem or noise interference that cannot be solved, consider relocation. Background noise and poor audio quality can ruin your audio. Audiences are more likely to tolerate bad video than they are bad audio.

---

## ALWAYS USE HEADPHONES

It cannot be stressed enough. Headphones allow you to hear everything that your camera or recorder picks up (or nothing, if you forgot to switch on your mic). You will hear whether you are recording properly, and quickly be able to detect and solve any problems you might encounter, such as wind or additional noise. Wind interference, for example, can ruin your interview and render your entire shot worthless.

---

## TEST RECORDING FIRST

Check your sound immediately after recording. This will ensure that your equipment is in good working order. When conducting an interview, be sure to conduct a test, because once you are recording live, you can’t recreate what you failed to record!

---

## TURN YOUR BACK ON NOISE

If you are recording your narration on location and picking up noise you don’t want, your body can form an obstacle between you and the noise. This may be less effective with an omni-directional microphone. If you record with a cardioid or shotgun microphone, you can mitigate sound considerably by turning your back (and repositioning your subject accordingly) towards the source of noise.

---

## STABILIZE AND PROTECT THE MIC

Always use a shock-mount to protect your microphone from movement. When holding the a microphone take care not to move your hand to avoid the noise of your hand on the handle. If outdoors, protect your microphone from wind (use a windscreen) or rain. Microphones are delicate and pick up any sound, including vibration and wind.

---
