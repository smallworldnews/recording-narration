# Recording Narration

![](images/narration_cover.jpg)

Scripting and recording a good voiceover is a professional skill that is developed over time, with practice. By following this lesson, you will begin to master this seemingly simple-- but actually quite difficult -- part of making a video.
